const RideTranslations = {
  template: {
    'datePicker-descriptor': {
      zho: '到',
      spa: 'To',
      eng: 'To',
    },
  },
  rides: {
    'tips-flight-number': {
      zho: '如果是系统不存在的航班，请只输入航班号',
      spa: 'please input only flight number for flight not found in our system.',
      eng: 'please input only flight number for flight not found in our system.',
    },
    tips: {
      zho: '在上车前您是否还要预留一些事情的时间？',
      spa: 'Where are you likely to pass from the plane to the arrival hall?',
      eng: 'Where are you likely to pass from the plane to the arrival hall?',
    },
    'all-label': {
      zho: '全选',
      spa: 'All',
      eng: 'All',
    },
    'baggage-claim-label': {
      zho: '取行李',
      spa: 'Baggage claim',
      eng: 'Baggage claim',
    },
    'customs-label': {
      zho: '海关',
      spa: 'Customs',
      eng: 'Customs',
    },
    descriptions: {
      zho: '需要的时间（分钟）',
      spa: 'Time required(minutes)',
      eng: 'Time required(minutes)',
    },
    'save-button': {
      zho: '保存',
      spa: 'Save',
      eng: 'Save',
    },

    'save-btn-destination': {
      zho: '保存',
      spa: 'save',
      eng: 'save',
    },
    'previous-flight-not-found-start': {
      zho: '请注意我们的系统目前无法找到当前航班信息。不过，如果您认为当前航班正确无误，您仍然可以使用它。',
      spa: 'Note that the system cannot trace this flight information.However,you can still use this flight if you are sure it is correct.',
      eng: 'Note that the system cannot trace this flight information.However,you can still use this flight if you are sure it is correct.',
    },
    'save-btn-start': {
      zho: '保存',
      spa: 'save',
      eng: 'save',
    },
    title: {
      zho: '身份验证',
      spa: 'Identity information verification',
      eng: 'Identity information verification',
    },
    'tips-for-24hours': {
      zho: '取消24小时之内的订单或者已使用5%优惠券的订单，不会退款',
      spa: 'Refund is not available for rides within 24 hours or rides with 5% discount applied',
      eng: 'Refund is not available for rides within 24 hours or rides with 5% discount applied',
    },
    'confirm-message': {
      zho: '请确认',
      spa: 'Please confirm order',
      eng: 'Please confirm order',
    },
    'agency-reference': {
      zho: '旅行社编号',
      spa: 'Clave de agencia',
      eng: 'Agency Reference',
    },
    'datePicker-descriptor': {
      zho: '到',
      spa: 'To',
      eng: 'To',
    },
    'future-tab': {
      zho: '未来',
      spa: 'Future',
      eng: 'Future',
    },
    'past-tab': {
      zho: '过去',
      spa: 'Past',
      eng: 'Past',
    },
    'search-future-list': {
      zho: '搜索',
      spa: 'Search',
      eng: 'Search',
    },
    'search-past-list': {
      zho: '搜索',
      spa: 'Search',
      eng: 'Search',
    },
    unpaid: {
      zho: '未支付',
      spa: 'Unpaid',
      eng: 'Unpaid',
    },
    paid: {
      zho: '已支付',
      spa: 'Paid',
      eng: 'Paid',
    },
    cancelled: {
      zho: '已取消',
      spa: 'Cancelled',
      eng: 'Cancelled',
    },
    'start-datetime-descriptor': {
      zho: '本地时间',
      spa: 'Local time at',
      eng: 'Local time at',
    },
    'duration-descriptor': {
      zho: '时长',
      spa: 'duration',
      eng: 'duration',
    },
    'duration-hours': {
      zho: '小时',
      spa: 'hours',
      eng: 'hours',
    },
    'my-dialog-tips': {
      zho: '抱歉，订单已取消',
      spa: 'Sorry, the order has been cancelled',
      eng: 'Sorry, the order has been cancelled',
    },
    'card-title-passenger': {
      zho: '乘客',
      spa: 'Pasajeros',
      eng: 'Passenger',
    },
    'message-text': {
      zho: '备注',
      spa: 'Message',
      eng: 'Message',
    },
    'driver-sign-text': {
      zho: '接机指示牌',
      spa: 'Driver Sign',
      eng: 'Driver Sign',
    },

    'meeting-at-text': {
      zho: '上车点',
      spa: 'Meeting at',
      eng: 'Meeting at',
    },
    cell: {
      zho: '手机号',
      spa: 'Móvil',
      eng: 'Cell',
    },
    customer: {
      zho: '乘车人',
      spa: 'Cliente',
      eng: 'Customer',
    },
    agency: {
      zho: '旅行社编号',
      spa: 'Clave de agencia',
      eng: 'Agency Ref',
    },
    'ride-bless': {
      zho: '，祝您旅途愉快!',
      spa: '¡Que tengas un bien viaje!',
      eng: 'Have a nice and safe trip！',
    },

    'ride-id-lbl': {
      zho: '订单号',
      spa: 'Viaje',
      eng: 'Ride ',
    },
    'local-time': {
      zho: '当地时间',
      spa: 'Reservado el',
      eng: 'Local time',
    },

    duration: {
      zho: '用车时间',
      spa: 'duration',
      eng: 'Duration',
    },
    hour: {
      zho: '小时。',
      spa: 'hour.',
      eng: 'hour.',
    },
    'contact-info-title': {
      zho: '联系人信息',
      spa: 'Información de contacto',
      eng: 'Contact Info',
    },

    'comments-title': {
      zho: '留言',
      spa: 'Comentarios',
      eng: 'Comments',
    },

    'meet-box': {
      zho: '接机指示牌',
      spa: 'Letrero de Recepción',
      eng: 'Meet and greet sign',
    },
    'note-box': {
      zho: '备注',
      spa: 'Notas para el chofer',
      eng: 'Notes to driver',
    },
    'contact-us': {
      zho: '联系我们',
      spa: 'Contáctanos',
      eng: 'Contact us',
    },

    'waiting-time-title': {
      zho: '等待时长',
      spa: 'Tiempo de espera',
      eng: 'Waiting time',
    },
    'waiting-time-statement': {
      zho: '驾驶员到达目的地后，免费等待时长为60分钟',
      spa: '60 minutos de espera después de la llegada de tu vuelo incluidos',
      eng: '60 minutes of waiting after the arrival of your flight is included',
    },
    'free-cancel-title': {
      zho: '免费取消',
      spa: 'Cancelación gratuita',
      eng: 'Free cancellation',
    },
    'free-cancel-statement': {
      zho: '如果在订单开始前24小时或者更早取消订单，您将得到全额退款',
      spa: 'Reembolso total al cancelar 24 o más horas antes del servicio',
      eng: 'Full refund if cancelled 24 hours or more before the actual ride.',
    },

    'before-dist': {
      zho: '预计行驶',
      spa: 'Estimado entre',
      eng: 'Estimated at',
    },

    'duration-mm-unit': {
      zho: '分钟',
      spa: 'minutos',
      eng: 'minutes',
    },

    'funds-detail-title': {
      zho: '费用明细',
      spa: 'Detalle',
      eng: 'Detail',
    },

    'vehicle-serving-fee-lbl': {
      zho: '车辆服务费',
      spa: 'Vehículo seleccionado',
      eng: 'Vehicle service',
    },
    'meet-n-greet-fee-lbl': {
      zho: '机场接机举牌',
      spa: 'Recepción Personalizada',
      eng: 'Meet and greet',
    },
    'total-left': {
      zho: '总额',
      spa: 'Precio Total',
      eng: 'Total price',
    },

    'pay-notify': {
      zho: '支付将跳转到全球第一支付平台stripe,我司不会获取您的银行账号，请放心使用。',
      spa: 'Serás dirigido a Stripe, empresa de servicios financieros líder en EU, para realizar un pago seguro',
      eng: 'You will be redirected to Stripe, a leading US financial service company, to make a secure payment.',
    },

    'pay-warning': {
      zho: '点击支付表示您已阅读并同意接受',
      spa: 'Al dar clic en "Pagar", aceptas haber leído los',
      eng: 'By clicking Pay, you indicate that you have read and agreed to',
    },

    'book-notice-link': {
      zho: '《订阅须知》',
      spa: '《Términos de Reserva》',
      eng: '《Booking Notice》',
    },

    'pay-btn': {
      zho: '去支付',
      spa: 'Pagar',
      eng: 'Pay',
    },

    'driver-info-title': {
      zho: '驾驶员信息',
      spa: 'Información del conductor',
      eng: 'Driver information',
    },
    driver: {
      zho: '驾驶员',
      spa: 'Conductor',
      eng: 'Driver',
    },
    meeting: {
      zho: '上车点',
      spa: 'Meeting at',
      eng: 'Meeting at',
    },

    'engage-round-trip': {
      zho: '预订往返行程',
      spa: 'Reserva tu viaje de retorno',
      eng: 'Book return trip',
    },
    'pay-btn-text': {
      zho: '支付',
      spa: 'Pago',
      eng: 'Pay',
    },
    'payment-title': {
      zho: '支付窗口',
      spa: 'Payment',
      eng: 'Payment',
    },
    'payment-cancel': {
      zho: '取消',
      spa: 'Cancelar',
      eng: 'Cancel',
    },
    'payment-ok': {
      zho: '确定',
      spa: 'OK',
      eng: 'OK',
    },
    'pay-success-lbl': {
      zho: '您的订单支付成功，订单号：',
      spa: 'Payment succeeded, Ride',
      eng: 'Payment succeeded, Ride',
    },
    'pay-status-lbl': {
      zho: '订单加载失败，您的订单号是',
      spa: 'Whoops, something went wrong. Please contact customer support and your ride is',
      eng: 'Whoops, something went wrong. Please contact customer support and your ride is',
    },
    'pay-status-contact': {
      zho: '，请联系客服。',
      spa: ', Please contact customer service.',
      eng: ', Please contact customer service.',
    },
    'pay-status-redirect': {
      zho: '确定',
      spa: 'OK',
      eng: 'OK',
    },
    'pay-fail-lbl': {
      zho: '您的订单支付失败：',
      spa: 'Payment did not go through: ',
      eng: 'Payment did not go through: ', // the reason
    },
    'pay-fail-retry': {
      zho: '重试',
      spa: 'Retry',
      eng: 'Retry',
    },
    'cancel-ride': {
      zho: '取消订单',
      spa: 'Cancelar Viaje',
      eng: 'Cancel Ride',
    },
    'view-list': {
      zho: '订单列表',
      eng: 'View List',
      spa: 'View List',
    },
    'sms-code-title': {
      zho: '输入短信验证码',
      spa: 'Introduce el código de confirmación que recibiste por SMS',
      eng: 'Enter SMS verification code',
    },
    'sms-number-title': {
      zho: '请输入手机号码',
      spa: 'Enter Cellphone Number',
      eng: 'Enter Cellphone Number',
    },
    'sms-sent': {
      zho: '短信验证码已发送至',
      spa: 'Se ha enviado el código de confirmación por SMS a',
      eng: 'SMS verification code has been sent to',
    },
    'sms-qs': {
      zho: '没收到验证码？',
      spa: '¿No recibiste el Código de confirmación?',
      eng: "Didn't receive the verification code?",
    },
    'sms-resend-text': {
      zho: '重新发送',
      spa: 'Reenviar código',
      eng: 'Resend',
    },
    'sms-cancel': {
      zho: '取消',
      spa: 'Cancelar',
      eng: 'Cancel',
    },
    'sms-ok-text': {
      zho: '确定',
      spa: 'OK',
      eng: 'OK',
    },
    'sms-send-text': {
      zho: '发送验证码',
      spa: 'Enviar código de confirmación',
      eng: 'Send verification code',
    },
    'cancel-success-title': {
      zho: '订单已取消',
      spa: 'The order has been cancelled.',
      eng: 'The order has been cancelled.',
    },
    'cancel-success-content': {
      zho: '费用将在取消后3至10个工作日退还至原信用卡账户。',
      spa: 'The charge will be refunded to the original credit card account 3 to 10 working days after the cancellation.',
      eng: 'The charge will be refunded to the original credit card account 3 to 10 working days after the cancellation.',
    },
    'cancel-confirm': {
      zho: '确定',
      spa: 'OK',
      eng: 'OK',
    },
    'vali-empty': {
      zho: '验证码不能为空',
      spa: 'El código de confirmación no puede estar vacío',
      eng: 'Verification code cannot be empty',
    },
    'vali-format': {
      zho: '请输入8位验证码',
      spa: 'Por favor introduce el código de confirmación de 8 dígitos',
      eng: 'Please enter a 8-digit verification code',
    },
    'vali-match': {
      zho: '验证码不正确',
      spa: 'Cell phone verification code does not match.',
      eng: 'Cell phone verification code does not match.',
    },
    'unpaid-message': {
      zho: '请尽快付款以确认此订单',
      spa: 'Por favor, realiza el pago lo antes posible para confirmar el viaje',
      eng: 'Please pay as soon as possible to confirm this ride',
    },
    thanks: {
      zho: '感谢您使用我们的服务!',
      spa: '¡Gracias por confiar en nosotros!',
      eng: 'Thank you for choosing us!',
    },
    summary: {
      zho: '订单详情',
      spa: 'Resumen del viaje',
      eng: 'Ride Summary',
    },
    'departure-tab': {
      zho: '去程',
      spa: 'Ride Info',
      eng: 'Ride Info',
    },
    'return-tab': {
      zho: '返程',
      spa: 'Return Ride Info',
      eng: 'Return Ride Info',
    },
    'ride-info-title': {
      zho: '预订信息',
      spa: 'Información del viaje',
      eng: 'Ride info',
    },
    'ride-info-start': {
      zho: '起点',
      spa: 'Desde',
      eng: 'From',
    },
    'ride-info-end': {
      zho: '终点',
      spa: 'Hacia',
      eng: 'To',
    },
    'ride-info-passengers': {
      zho: '乘客',
      spa: 'Pasajeros',
      eng: 'Passengers',
    },
    'feedback-rates-tips': {
      zho: '请对此次服务进行评价',
      spa: 'Please comment on the ride service',
      eng: 'Please comment on the ride service',
    },
    'feedback-comment-tips': {
      zho: '您的意见',
      spa: 'Comment',
      eng: 'Comment',
    },
    'feedback-notice': {
      zho: '您的意见有助于改善我们的服务',
      spa: 'Please add your comments to help us improve our service',
      eng: 'Please add your comments to help us improve our service',
    },

    'feedback-submit': {
      zho: '提交',
      spa: 'Confirm',
      eng: 'Confirm',
    },
    'ride-info-luggage': {
      zho: '行李',
      spa: 'Maletas',
      eng: 'luggage',
    },
    'ride-info-date': {
      zho: '日期',
      spa: 'Fecha',
      eng: 'Date',
    },
    'ride-info-time': {
      zho: '时间',
      spa: 'Hora',
      eng: 'Time',
    },
    'vehicle-info-title': {
      zho: '车辆信息',
      spa: 'Vehículo',
      eng: 'Vehicle',
    },
    'passengers-max': {
      zho: '最多',
      spa: 'Max',
      eng: 'Max',
    },
    'luggage-max': {
      zho: '最多',
      spa: 'Max',
      eng: 'Max',
    },
    'driver-info-title': {
      zho: '驾驶员',
      spa: 'Conductor',
      eng: 'Driver',
    },
    'driver-name-text': {
      zho: '姓名',
      spa: 'Nombre',
      eng: 'Name',
    },
    'passenger-info-title': {
      zho: '联系方式',
      spa: 'Contacto',
      eng: 'Contact',
    },
    'passenger-info-name': {
      zho: '姓名',
      spa: 'Nombre',
      eng: 'Name',
    },
    'passenger-info-email': {
      zho: '邮箱',
      spa: 'Email',
      eng: 'E-mail',
    },
    'passenger-info-phone-number-1': {
      zho: '联系电话 1',
      spa: 'Teléfono 1',
      eng: 'Phone Number 1',
    },
    'passenger-info-phone-number-2': {
      zho: '联系电话 2',
      spa: 'Teléfono 2',
      eng: 'Phone Number 2',
    },
    'passenger-info-phone-number-3': {
      zho: '联系电话 3',
      spa: 'Teléfono 3',
      eng: 'Phone Number 3',
    },
    'passenger-info-phone-number-4': {
      zho: '联系电话 4',
      spa: 'Teléfono 4',
      eng: 'Phone Number 4',
    },
    'passenger-info-social-media': {
      zho: '社交媒体',
      spa: 'Modificar',
      eng: 'Social Media',
    },
    'additional-info-title': {
      zho: '其他信息',
      spa: 'Adicional',
      eng: 'Additional',
    },
    'additional-info-greet-sign-fee': {
      zho: '机场接机举牌',
      spa: 'Letrero de recepción',
      eng: 'Meet & Greet Sign',
    },
    'additional-info-sign-message': {
      zho: '举牌文字',
      spa: 'Comentarios',
      eng: 'Sign Message',
    },
    'additional-info-additional-seats': {
      zho: '额外座位',
      // todo: add Spanish
      spa: 'Additional Seats',
      eng: 'Additional Seats',
    },
    'additional-info-child-seat': {
      zho: '儿童座椅',
      // todo: add Spanish
      spa: 'Child Seat',
      eng: 'Child Seat',
    },
    'additional-info-assistance': {
      zho: '协助',
      // todo: add Spanish
      spa: 'Assistance',
      eng: 'Assistance',
    },
    'additional-info-notes': {
      zho: '备注',
      spa: 'Notas para el chofer',
      eng: 'Notes to driver',
    },
    'summary-total': {
      zho: '总计',
      spa: 'Total',
      eng: 'Total',
    },
    'footer-contact': {
      zho: '需要帮助？',
      spa: '¿Necesitas ayuda?',
      eng: 'Need help?',
    },
    'footer-contact-cta': {
      zho: '联系我们',
      spa: 'Contáctenos.',
      eng: 'Contact us.',
    },

    // 'unpaid-message': {
    //   zho: '订单已生成，请尽早完成付款，若超时未付款，系统将重新为您计算用车价格。',
    //   spa: 'The order has been generated. Please complete the payment as soon as possible. If you fail to make payment over time, the system will recalculate the car price for you.',
    //   eng: 'The order has been generated. Please complete the payment as soon as possible. If you fail to make payment over time, the system will recalculate the car price for you.'
    // },
    'unpaid-rise': {
      /// /
      zho: '临近用车时间，已为您更新价格，为确保您的用车服务正常进行，请尽早完成付款！',
      spa: 'The order has expired as the time for using the car approaches. To ensure your driving experience, please place an order and pay in advance.',
      eng: 'The order has expired as the time for using the car approaches. To ensure your driving experience, please place an order and pay in advance.',
    },
    'unpaid-invalid': {
      /// /
      zho: '临近用车时间，订单已失效。为确保您的用车体验，请您提前下单并付款。',
      spa: 'The order has expired as the time for using the car approaches. To ensure your driving experience, please place an order and pay in advance.',
      eng: 'The order has expired as the time for using the car approaches. To ensure your driving experience, please place an order and pay in advance.',
    },
    'unpaid-timeout': {
      /// /
      zho: '因超时未付款，订单已失效，请重新返回官网下单支付。',
      spa: 'The order has expired due to overdue payment. Please return to the official website to place an order for payment.',
      eng: 'The order has expired due to overdue payment. Please return to the official website to place an order for payment.',
    },
    'confirm-order': {
      zho: '确认订单',
      spa: 'Confirm the order',
      eng: 'Confirm the order',
    },

    'meeting-place': {
      zho: '上车点',
      eng: 'Meeting point',
      spa: 'Punto de encuentro',
    },
    socialMedia1: {
      zho: '社交媒体',
      spa: 'Modificar',
      eng: 'Social Media',
    },
    socialMedia2: {
      zho: '社交媒体',
      spa: 'Modificar',
      eng: 'Social Media',
    },
    socialMedia3: {
      zho: '社交媒体',
      spa: 'Modificar',
      eng: 'Social Media',
    },
    socialMedia4: {
      zho: '社交媒体',
      spa: 'Modificar',
      eng: 'Social Media',
    },
    phoneNumber1: {
      zho: '联系电话',
      spa: 'Teléfono',
      eng: 'Phone Number',
    },
    phoneNumber4: {
      zho: '联系电话',
      spa: 'Teléfono',
      eng: 'Phone Number',
    },
    phoneNumber3: {
      zho: '联系电话',
      spa: 'Teléfono',
      eng: 'Phone Number',
    },
    phoneNumber2: {
      zho: '联系电话',
      spa: 'Teléfono',
      eng: 'Phone Number',
    },
    'phone-number-text': {
      zho: '联系电话',
      spa: 'Teléfono',
      eng: 'Phone Number',
    },
    'confirm-ride-info': {
      zho: '预订信息',
      spa: 'Información del viaje',
      eng: 'Ride info',
    },
    'confirm-vehicle-info': {
      zho: '车辆信息',
      spa: 'Vehículo',
      eng: 'Vehicle',
    },
    'confirm-driver-info': {
      zho: '驾驶员',
      spa: 'Conductor',
      eng: 'Driver',
    },
    'confirm-passenger-info': {
      zho: '联系方式',
      spa: 'Contacto',
      eng: 'Contact',
    },
    'confirm-additional-info': {
      zho: '其他信息',
      spa: 'Adicional',
      eng: 'Additional',
    },
    'confirm-ok': {
      zho: '确定',
      spa: 'OK',
      eng: 'OK',
    },
  },
};
